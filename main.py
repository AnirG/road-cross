import pygame
import os
import random
import math
import configparser

pygame.init()


configparser = configparser.RawConfigParser()
configFilePath = os.path.join(os.path.dirname(__file__), 'myConfig.cfg')
configparser.read(configFilePath)
gameWidth  = configparser.getint("Info","Width")
gameHeight = configparser.getint("Info","Height")
myfont = configparser.get("Info","myfont")
#config.write(configFilePath)

pygame.font.init()

myFont = pygame.font.SysFont(myfont, 26)
readyFont = pygame.font.SysFont(myfont, 40)
screen = pygame.display.set_mode((gameWidth, gameHeight))

bg_load = pygame.image.load('rsz_bg_load.jpg')
car = pygame.image.load('autoCar.png')
inX = 0
inY = 350

for x in range(1, 850):
    screen.blit(bg_load,  (0, 0))
    screen.blit(car,  (inX, inY))
    txt = readyFont.render('Get Ready!',  True, (255, 255, 255))
    screen.blit(txt,  (300, 150))

    pygame.display.update()
    inX += 1

run = True
pygame.display.set_caption("Road Cross")

bg = pygame.image.load('bg_grass_textured.png')

player1Img = pygame.image.load('car1.png')
p1X = 300
p1Y = 540

player2Img = pygame.image.load('car2.png')
p2X = 300
p2Y = 0


def player1(p1X,  p1Y):
    screen.blit(player1Img,  (p1X,  p1Y))


def player2(p2X,  p2Y):
    screen.blit(player2Img,  (p2X,  p2Y))

static = []
dynamic = []


def static_obj():
    sobj1 = pygame.image.load('stone.png')
    screen.blit(sobj1,  (80, 398))
    static.append([80, 398])
    sobj2 = pygame.image.load('stone.png')
    screen.blit(sobj2,  (700, 290))
    static.append([700, 270])
    sobj3 = pygame.image.load('traffic-lights.png')
    screen.blit(sobj3,  (704,  8))
    static.append([704, 8])
    sobj4 = pygame.image.load('barrier.png')
    screen.blit(sobj4,  (125,  285))
    static.append([125, 285])
    sobj5 = pygame.image.load('cow_64.png')
    screen.blit(sobj5,  (260,  285))
    static.append([260, 285])
    sobj6 = pygame.image.load('barrier2.png')
    screen.blit(sobj6,  (530,  285))
    static.append([530, 285])
    sobj7 = pygame.image.load('stone.png')
    screen.blit(sobj7,  (740,  398))
    static.append([740, 398])
    sobj8 = pygame.image.load('stone.png')
    screen.blit(sobj8,  (654,  170))
    static.append([654, 170])
    sobj9 = pygame.image.load('stone.png')
    screen.blit(sobj9,  (400, 398))
    static.append([400, 398])
    sobj10 = pygame.image.load('stone.png')
    screen.blit(sobj10,  (600, 398))
    static.append([600, 398])
    sobj11 = pygame.image.load('stone.png')
    screen.blit(sobj11,  (240, 398))
    static.append([240, 398])
    sobj12 = pygame.image.load('stone.png')
    screen.blit(sobj12,  (20,  170))
    static.append([20, 170])
    sobj13 = pygame.image.load('stone.png')
    screen.blit(sobj13,  (340,  170))
    static.append([340, 170])


coordx_change = 0
coordy_change = 0


def isCollided(pX, pY, objX, objY):
    dx = abs(pX-objX)
    dy = abs(pY-objY)
    if dx <= 52 and dy <= 44:
        return True
    else:
        return False


obj1X = 6
obj2X = -100
obj3X = 736
obj4X = 1080
obj5X = 6
obj6X = -300


def moveObj1(vel):
    dobj1 = pygame.image.load('delivery-truckL.png')
    inRange = True
    while inRange:
        global obj1X
        screen.blit(dobj1,  (obj1X,  475))
        obj1X += 1.3*vel*0.75
        dynamic.append([obj1X, 475])
        inRange = False
        if obj1X >= 736:
            inRange = False
            obj1X = 6


def moveObj2(vel):
    dobj2 = pygame.image.load('excavators.png')
    inRange = True
    while inRange:
        global obj2X
        screen.blit(dobj2,  (obj2X,  177))
        obj2X += 0.3*vel*0.75
        dynamic.append([obj2X, 177])
        inRange = False
        if obj2X >= 736:
            inRange = False
            obj2X = 6


def moveObj3(vel):
    dobj3 = pygame.image.load('delivery-truckR.png')
    inRange = True
    while inRange:
        global obj3X
        screen.blit(dobj3,  (obj3X,  147))
        obj3X -= 0.8*vel*0.75
        dynamic.append([obj3X, 147])
        inRange = False
        if obj3X <= 6:
            inRange = False
            obj3X = 736


def moveObj4(vel):
    dobj4 = pygame.image.load('ambulanceR.png')
    inRange = True
    while inRange:
        global obj4X
        screen.blit(dobj4,  (obj4X,  147))
        obj4X -= 0.8*vel*0.75
        dynamic.append([obj4X, 147])
        inRange = False
        if obj4X <= 6:
            inRange = False
            obj4X = 736


def moveObj5(vel):
    dobj5 = pygame.image.load('carL.png')
    inRange = True
    while inRange:
        global obj5X
        screen.blit(dobj5,  (obj5X,  48))
        obj5X += 1.1*vel*0.75
        dynamic.append([obj5X, 48])
        inRange = False
        if obj5X >= 736:
            inRange = False
            obj5X = 6


def moveObj6(vel):
    dobj6 = pygame.image.load('automobileL.png')
    inRange = True
    while inRange:
        global obj6X
        screen.blit(dobj6,  (obj6X,  48))
        obj6X += 1.1*vel*0.75
        dynamic.append([obj6X, 48])
        inRange = False
        if obj6X >= 736:
            inRange = False
            obj6X = 6


rsz_gover = pygame.image.load('rsz_gover.jpg')

fps = pygame.time.Clock()

f = 0
pState = 0
p1Level = 1
p2Level = 1
gOver1 = 0
gOver2 = 0
score1 = 0
score2 = 0
totalScore1 = 0
totalScore2 = 0
timePlayer1 = 0
timePlayer2 = 0


def calcScore1(y):
    global score1
    if y <= 475:
        score1 = 10
    if y <= 398:
        score1 = 30
    if y <= 378:
        score1 = 35
    if y <= 290:
        score1 = 40
    if y <= 285:
        score1 = 50
    if y <= 177:
        score1 = 60
    if y <= 170:
        score1 = 70
    if y <= 147:
        score1 = 95
    if y <= 48:
        score1 = 115
    if y <= 8:
        score1 = 120


def calcScore2(y):
    global score2
    if y >= 475:
        score2 = 120
    elif y >= 398:
        score2 = 110
    elif y >= 378:
        score2 = 90
    elif y >= 290:
        score2 = 85
    elif y >= 285:
        score2 = 80
    elif y >= 177:
        score2 = 70
    elif y >= 170:
        score2 = 60
    elif y >= 147:
        score2 = 50
    elif y >= 48:
        score2 = 25
    elif y >= 8:
        score2 = 5


def reset():
    global f
    global pState
    global p1Level
    global p2Level
    global gOver1
    global gOver2
    global score1
    global score2
    global totalScore1
    global totalScore2
    global timePlayer1
    global timePlayer2
    global run
    f = 0
    pState = 0
    p1Level = 1
    p2Level = 1
    gOver1 = 0
    gOver2 = 0
    score1 = 0
    score2 = 0
    totalScore1 = 0
    totalScore2 = 0
    timePlayer1 = 0
    timePlayer2 = 0
    run = True

overrun = True

while overrun:
    while run:
        dynamic.clear()
        fps.tick(120)
        screen.blit(bg, (0, 0))
        static_obj()
        if gOver1 == 1 and gOver2 == 0:
            pState = 1
        elif gOver1 == 0 and gOver2 == 1:
            pState = 0
        if pState == 0 and gOver1 == 0:
            timePlayer1 += 0.018
            moveObj1(p1Level)
            moveObj2(p1Level)
            moveObj3(p1Level)
            moveObj4(p1Level)
            moveObj5(p1Level)
            moveObj6(p1Level)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        coordx_change = 1.2*p1Level*0.75
                    if event.key == pygame.K_UP:
                        coordy_change = -1.2*p1Level*0.75
                    if event.key == pygame.K_LEFT:
                        coordx_change = -1.2*p1Level*0.75
                    if event.key == pygame.K_DOWN:
                        coordy_change = 1.2*p1Level*0.75
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        coordx_change = 0
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                        coordy_change = 0

            p1X += coordx_change
            p1Y += coordy_change

            if p1X <= 0:
                p1X = 0
            elif p1X >= 736:
                p1X = 736
            if p1Y <= 0:
                timePlayer1 = round((800/timePlayer1), 2)
                totalScore1 += timePlayer1
                timePlayer1 = 0
                p1Y = 540
                p1Level += 1
                totalScore1 += 120
                pState = 1
                coordy_change = 0
                coordx_change = 0
            elif p1Y >= 540:
                p1Y = 540

            player1(p1X,  p1Y)

            score1 = 0
            calcScore1(p1Y)

            levelp1Text = myFont.render('Level: ',  False,  (255, 0, 0))
            levelp1 = myFont.render(str(p1Level),  False,  (255, 0, 0))
            screen.blit(levelp1Text, (600, 540))
            screen.blit(levelp1, (670, 540))

            scorep1Text = myFont.render('Score: ',  False,  (255, 0, 0))
            scorep1 = myFont.render(str(score1),  False,  (255, 0, 0))
            screen.blit(scorep1Text, (20, 540))
            screen.blit(scorep1, (110, 540))

            for i in range(len(static)):
                col1 = isCollided(p1X, p1Y,  static[i][0],  static[i][1])
                if col1:
                    p1X = 300
                    p1Y = 540
                    col1 = False
                    pState = 1
                    f += 1
                    coordx_change = 0
                    coordy_change = 0
                    totalScore1 += score1
                    gOver1 = 1

            for i in range(len(dynamic)):
                col2 = isCollided(p1X, p1Y,  dynamic[i][0],  dynamic[i][1])
                if col2:
                    p1X = 300
                    p1Y = 540
                    col2 = False
                    pState = 1
                    f += 1
                    coordx_change = 0
                    coordy_change = 0
                    totalScore1 += score1
                    gOver1 = 1

        elif pState == 1 and gOver2 == 0:
            timePlayer2 += 0.018
            moveObj1(p2Level)
            moveObj2(p2Level)
            moveObj3(p2Level)
            moveObj4(p2Level)
            moveObj5(p2Level)
            moveObj6(p2Level)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_d:
                        coordx_change = 1.2*p2Level*0.75
                    if event.key == pygame.K_w:
                        coordy_change = -1.2*p2Level*0.75
                    if event.key == pygame.K_a:
                        coordx_change = -1.2*p2Level*0.75
                    if event.key == pygame.K_s:
                        coordy_change = 1.2*p2Level*0.75
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_a or event.key == pygame.K_d:
                        coordx_change = 0
                    if event.key == pygame.K_w or event.key == pygame.K_s:
                        coordy_change = 0
            p2X += coordx_change
            p2Y += coordy_change
            if p2X <= 0:
                p2X = 0
            elif p2X >= 736:
                p2X = 736
            if p2Y <= 0:
                p2Y = 0
            elif p2Y >= 540:
                timePlayer2 = round((800/timePlayer2),  2)
                totalScore2 += timePlayer2
                timePlayer2 = 0
                p2Y = 0
                p2Level += 1
                totalScore2 += 120
                pState = 0
                coordy_change = 0
                coordx_change = 0
            player2(p2X,  p2Y)
            score2 = 0
            calcScore2(p2Y)
            levelp2Text = myFont.render('Level: ',  False,  (255, 0, 0))
            levelp2 = myFont.render(str(p2Level),  False,  (255, 0, 0))
            screen.blit(levelp2Text, (600, 540))
            screen.blit(levelp2, (670, 540))
            scorep2Text = myFont.render('Score: ',  False,  (255, 0, 0))
            scorep2 = myFont.render(str(score2),  False,  (255, 0, 0))
            screen.blit(scorep2Text, (20, 540))
            screen.blit(scorep2, (110, 540))
            for i in range(len(static)):
                col1 = isCollided(p2X, p2Y,  static[i][0],  static[i][1])
                if col1:
                    p2X = 300
                    p2Y = 0
                    col1 = False
                    pState = 0
                    f += 1
                    coordx_change = 0
                    coordy_change = 0
                    totalScore2 += score2
                    gOver2 = 1

            for i in range(len(dynamic)):
                col2 = isCollided(p2X, p2Y,  dynamic[i][0],  dynamic[i][1])
                if col2:
                    p2X = 300
                    p2Y = 0
                    col2 = False
                    pState = 0
                    f += 1
                    coordx_change = 0
                    coordy_change = 0
                    totalScore2 += score2
                    gOver2 = 1

        if gOver1 == 1 and gOver2 == 1:
            run = False
        pygame.display.update()
       
    for x in range(800):
        screen.fill((255, 255, 255))
        screen.blit(rsz_gover, (0, 0))
        totalp1Text = myFont.render('Player 1: ', True,  (255, 0, 0))
        p1 = myFont.render(str(totalScore1) + " points", True,  (255, 0, 0))
        screen.blit(totalp1Text, (100, 320))
        screen.blit(p1, (220, 320))
        totalp2Text = myFont.render('Player 2: ', True,  (255, 0, 0))
        p2 = myFont.render(str(totalScore2) + " points", True,  (255, 0, 0))
        screen.blit(totalp2Text, (430, 320))
        screen.blit(p2, (560, 320))
        if totalScore2 < totalScore1:
            w1 = myFont.render('Player 1 wins! ', True,  (0, 255, 0))
            screen.blit(w1, (300, 450))
        elif totalScore2 > totalScore1:
            w2 = myFont.render('Player 2 wins! ', True,  (0, 255, 0))
            screen.blit(w2, (300, 450))
        else:
            draw = myFont.render('Draw! ', True,  (0, 255, 0))
            screen.blit(draw, (340, 420))
        pygame.display.flip()

    reset()
        

pygame.quit()
