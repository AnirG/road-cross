
The game is called "Road Cross".

If you get bored of playing with the parameters that I set, there's a config file along with this directory. You can change the parameters the way you want.

Rules of the game:
1. 5 points for crossing a fixed obstacle.
2. 10 points for crossing a moving obstacle.
3. The player reaching the other end of the road with lesser time has an advantage in terms of points. A player is awarded points based on time using a secret formula (lol!).
4. The speed of all the obstacles increase as you level up including your car.
5. Player1 and player2 play alternatively with increased speed if none of them dies. If a player dies, the other play continously play until he dies.
6. The game continues until both of the players die. 
7. Final score is calculated by adding the score in every levels for a player (which is the sum total of points gained by defending obstacles and points awarded due to time.)

Enjoy playing!
